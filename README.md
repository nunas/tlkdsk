Calls Web Service Project and API guidelines
====================

These guidelines will help you install and test the API and Client Interface.

* [Stack](#stack)
* [Installation](#installation)
* [Calls API](#calls-api)
* [Client UI](#client-ui)

## Stack

Stack chosen to complete this test:

* Client
	* Swagger UI
	
* Server
	* Tomcat 9
	* Java 8
	* JAX-RS SPEC + RESTEasy Implementation (Web Services)
	* Log4j2 (Logging)
	* JDBC (DB connection)
	
* Database
	* PostgreSQL 12
	* c3p0 (Connection Pooling)

## Installation (Windows Platform)
To install and run the API and Client Interface:

1. 
	* Use the provided war files OR
	* Clone this repository at https://nunas@bitbucket.org/nunas/tlkdsk.git
2. 	Update the database configs at backend-test-ws\src\main\resources\database.properties (OPTIONAL, default is localhost)
	If modified don't forget to update Swagger Server config (ICallWS.java -> @Server annotation)
3. 	Run database script located at backend-test-core\sql\01-create-table.sql
4. 	Deploy backendtest-client and backendtest-ws and run the applications accordingly

More information about the client and API enpoints in the following sections.
	
## Calls API

* Endpoints

	* create calls [@POST]: http://host:port/backendtest-ws/api/call/create
	* delete call [@DELETE]: http://host:port/backendtest-ws/api/call/delete/{id}
	* get all calls [@GET]: http://host:port/backendtest-ws/api/call/allCalls
	* get statistics [@GET]: http://host:port/backendtest-ws/api/call/statistics

## Client UI

* Endpoint

	* http://host:port/backendtest-client/client-ui/index.html