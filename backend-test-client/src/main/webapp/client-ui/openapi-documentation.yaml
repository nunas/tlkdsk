openapi: 3.0.1
info:
  title: Calls Web Service
  description: API for the Calls Web Service
  contact:
    name: Nuno Oliveira
    email: nunmigoliveira@gmail.com
  version: 1.0.0
servers:
- url: http://localhost:8080/backendtest-ws
  description: Localhost server
  variables: {}
tags:
- name: Calls API
  description: Requested API for the take home test
paths:
  /api/call/delete/{id}:
    delete:
      tags:
      - Calls API
      summary: Deletes the call uniquely identified by id
      operationId: deleteCall_1
      parameters:
      - name: id
        in: path
        description: The call unique identifier
        required: true
        schema:
          type: integer
          format: int64
      responses:
        200:
          description: In case of success
          content:
            text/plain: {}
        404:
          description: In case of call not found
          content:
            text/plain: {}
        500:
          description: In case of unexpected error
          content:
            text/plain: {}
  /api/call/create:
    post:
      tags:
      - Calls API
      summary: Creates a list of calls
      operationId: createCalls_1
      requestBody:
        description: List of calls to be created. At least a list with 1 element is
          required
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Call'
        required: true
      responses:
        200:
          description: In case of success
          content:
            text/plain: {}
        400:
          description: In case of business error
          content:
            text/plain: {}
        500:
          description: In case of unexpected error
          content:
            text/plain: {}
  /api/call/statistics:
    get:
      tags:
      - Calls API
      summary: 'Returns a statistic compilation, with the following information: Total
        call duration by type; Total number of calls; Number of calls by Caller Number;Number
        of calls by Callee Number; Total call cost;'
      operationId: getStatistics_1
      responses:
        200:
          description: In case of success
          content:
            application/json: {}
        500:
          description: In case of unexpected error
          content:
            text/plain: {}
  /api/call/allCalls:
    get:
      tags:
      - Calls API
      summary: Returns all calls, taking into account the call type, and for pagination
        cases the offset and limit values
      operationId: getCalls_1
      parameters:
      - name: type
        in: query
        description: The call type
        schema:
          type: string
          enum:
          - INBOUND
          - OUTBOUND
        example: INBOUND
      - name: offset
        in: query
        description: The first element index to be retrieved
        schema:
          type: integer
          format: int64
        example: 5
      - name: limit
        in: query
        description: Maximum elements to retrieve
        schema:
          type: integer
          format: int64
        example: 10
      responses:
        200:
          description: In case of success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Call'
        404:
          description: In case of negative offset or limit values
          content:
            text/plain: {}
        500:
          description: In case of unexpected error
          content:
            text/plain: {}
components:
  schemas:
    Call:
      required:
      - callType
      - calleeNumber
      - callerNumber
      - endDate
      - startDate
      type: object
      properties:
        id:
          type: integer
          format: int64
        callerNumber:
          maxLength: 32
          type: string
          description: The caller number
          example: "00351910000000"
        calleeNumber:
          maxLength: 32
          type: string
          description: The callee number
          example: "00351930000000"
        callType:
          type: string
          description: The call type
          example: OUTBOUND
          enum:
          - INBOUND
          - OUTBOUND
        startDate:
          type: string
          description: The call start date
          format: date-time
          example: "2019-11-14 09:43:42"
        endDate:
          type: string
          description: The call end date
          format: date-time
          example: "2019-11-14 09:47:11"
      description: Model representing the Call database entity
