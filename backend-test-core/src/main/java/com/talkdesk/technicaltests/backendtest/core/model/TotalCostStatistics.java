package com.talkdesk.technicaltests.backendtest.core.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.talkdesk.technicaltests.backendtest.core.enums.CallTypeEnum;

public class TotalCostStatistics {

	private static final BigDecimal TEN_CENTS = new BigDecimal(0.10).setScale(2, BigDecimal.ROUND_HALF_UP);
	private static final BigDecimal FIVE_CENTS = new BigDecimal(0.05).setScale(2, BigDecimal.ROUND_HALF_UP);
	
	private static final BigDecimal FIVE_MINUTES_SECONDS = new BigDecimal(300);
	private static final BigDecimal ONE_MINUTE_SECONDS = new BigDecimal(60);
	
	@JsonIgnore
	private Date startDate;
	private CallTypeEnum callType;
	private String callDuration;
	@JsonIgnore
	private long callDurationSeconds;
	private BigDecimal totalDayCost;
	
	public TotalCostStatistics() {
	}
	
	public TotalCostStatistics(ResultSet resultSet) throws SQLException {
		int i = 0;
		
		this.startDate = resultSet.getDate(++i);
		this.callType = CallTypeEnum.valueOf(resultSet.getString(++i));
		this.callDurationSeconds = resultSet.getLong(++i);
		this.callDuration = resultSet.getString(++i);
		
		this.setTotalDayCost();
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public CallTypeEnum getCallType() {
		return callType;
	}

	public void setCallType(CallTypeEnum callType) {
		this.callType = callType;
	}
	
	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public long getCallDurationSeconds() {
		return callDurationSeconds;
	}

	public void setCallDurationSeconds(long callDurationSeconds) {
		this.callDurationSeconds = callDurationSeconds;
	}

	public BigDecimal getTotalDayCost() {
		return this.totalDayCost;
	}

	public void setTotalDayCost() {
		BigDecimal totalCost = BigDecimal.ZERO;
		BigDecimal fiveMinutes = new BigDecimal(5);
		
		if(CallTypeEnum.OUTBOUND.equals(this.getCallType())) {
			
			BigDecimal callDurationSeconds = new BigDecimal(this.getCallDurationSeconds());
			BigDecimal remainderMinutes = BigDecimal.ZERO;
			if(callDurationSeconds.compareTo(FIVE_MINUTES_SECONDS) <= 0 ) {
				
				//rounded minutes within first 5 minutes period
				remainderMinutes = callDurationSeconds.divide(ONE_MINUTE_SECONDS, 0, BigDecimal.ROUND_HALF_UP);
				
				totalCost = remainderMinutes.multiply(TEN_CENTS);
			} else {
				//5 minutes call cost
				BigDecimal fiveMinutesCost = fiveMinutes.multiply(TEN_CENTS);
				//seconds after first five minutes
				BigDecimal secondsAtFiveCents = callDurationSeconds.subtract(FIVE_MINUTES_SECONDS);
				
				//rounded remainder minutes after first 5 minutes
				remainderMinutes = secondsAtFiveCents.divide(ONE_MINUTE_SECONDS, 0, BigDecimal.ROUND_HALF_UP);
				
				totalCost = fiveMinutesCost.add(remainderMinutes.multiply(FIVE_CENTS));
			}
		}
		
		this.totalDayCost = totalCost;
	}
}
