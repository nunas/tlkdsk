package com.talkdesk.technicaltests.backendtest.core.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.common.PersistenceHelper;
import com.talkdesk.technicaltests.backendtest.core.model.Call;

/**
 * Data Access Object that manages the Call entities
 * 
 * @author nmoliveira
 *
 */
public class CallDAO extends PersistenceHelper<Call> {
	
	/**
	 * Singleton Approach
	 */
	
	private static CallDAO callDAO = null;

	private CallDAO() {
	}

	public static CallDAO getInstance() {
		if (callDAO == null) {
			callDAO = new CallDAO();
		}
		return callDAO;
	}
	
	public int createCall(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException {
		int insertedRows = super.persist(queryBuilder, queryParams, connection);
		
		return insertedRows;
	}
	
	public int deleteCall(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException {
		int affectedRows = super.persist(queryBuilder, queryParams, connection);
		
		return affectedRows;
	}
	
	public List<Call> getCalls(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException {
		List<Call> calls = super.getList(queryBuilder, queryParams, connection);
		
		return calls;
	}
	
	@Override
	public Call toEntity(ResultSet resultSet) throws SQLException {
		return new Call(resultSet);
	}

}
