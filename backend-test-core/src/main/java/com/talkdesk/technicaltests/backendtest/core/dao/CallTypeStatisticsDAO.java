package com.talkdesk.technicaltests.backendtest.core.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.common.PersistenceHelper;
import com.talkdesk.technicaltests.backendtest.core.model.CallTypeStatistics;

/**
 * Data Access Object that manages the CallTypeStatistics entities
 * 
 * @author nmoliveira
 *
 */
public class CallTypeStatisticsDAO extends PersistenceHelper<CallTypeStatistics> {
	
	/**
	 * Singleton Approach
	 */
	
	private static CallTypeStatisticsDAO callTypeStatisticsDAO = null;

	private CallTypeStatisticsDAO() {
	}

	public static CallTypeStatisticsDAO getInstance() {
		if (callTypeStatisticsDAO == null) {
			callTypeStatisticsDAO = new CallTypeStatisticsDAO();
		}
		return callTypeStatisticsDAO;
	}
	
	public List<CallTypeStatistics> getCallTypeStatistics(StringBuilder queryBuilder, Connection connection) throws SQLException {
		List<CallTypeStatistics> callTypeStatistics = super.getList(queryBuilder, connection);
		
		return callTypeStatistics;
	}
	
	@Override
	public CallTypeStatistics toEntity(ResultSet resultSet) throws SQLException {
		return new CallTypeStatistics(resultSet);
	}

}
