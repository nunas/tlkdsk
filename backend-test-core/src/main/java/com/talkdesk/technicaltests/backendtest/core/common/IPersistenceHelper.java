package com.talkdesk.technicaltests.backendtest.core.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.exception.UnexpectedNumberOfElementsException;

/**
 * Interface holding the main methods to implement in order to interact with any database flavour
 * 
 * @author nmoliveira
 *
 * @param <T> the database entity
 */
public interface IPersistenceHelper<T> {

	/**
	 * 
	 * @param queryBuilder a StringBuilder object holding the query
	 * @param connection the connection reference to perform the queries
	 * 
	 * @return a single entity T
	 * 
	 * @throws SQLException in case of unexpected error
	 * @throws UnexpectedNumberOfElementsException if the query returned a number of results different from 1
	 */
	public T getSingleResult( StringBuilder queryBuilder, Connection connection ) throws SQLException, UnexpectedNumberOfElementsException;
	
	/**
	 * 
	 * @param queryBuilder a StringBuilder object holding the query
	 * @param queryParams a list of query parameters
	 * @param connection the connection reference
	 * 
	 * @return a single entity T
	 * 
	 * @throws SQLException in case of unexpected error
	 * @throws UnexpectedNumberOfElementsException if the query returned a number of results different from 1
	 */
	public T getSingleResult( StringBuilder queryBuilder, List<Object> queryParams, Connection connection ) throws SQLException, UnexpectedNumberOfElementsException;

	/**
	 * 
	 * @param queryBuilder a StringBuilder object holding the query
	 * @param connection the connection reference
	 * 
	 * @return a list of entities of type T
	 * 
	 * @throws SQLException in case of unexpected error
	 * @throws UnexpectedNumberOfElementsException if the query returned a number of results different from 1
	 */
	public List<T> getList( StringBuilder queryBuilder, Connection connection ) throws SQLException;
	
	/**
	 *
	 * @param queryBuilder a StringBuilder object holding the query
	 * @param queryParams a list of query parameters
	 * @param connection the connection reference
	 * 
	 * @return a list of entities of type T
	 * 
	 * @throws SQLException in case of unexpected error
	 * @throws UnexpectedNumberOfElementsException if the query returned a number of results different from 1
	 */
	public List<T> getList( StringBuilder queryBuilder, List<Object> queryParams, Connection connection ) throws SQLException;
	
	/**
	 * Performs a data modification operation and returns the number of affected rows
	 * 
	 * @param queryBuilder a StringBuilder object holding a data modification statement
	 * @param queryParams a list of query parameters
	 * @param connection the connection reference
	 * 
	 * @return the number of affected rows
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public int persist(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException;
}
