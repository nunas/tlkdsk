package com.talkdesk.technicaltests.backendtest.core.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.exception.UnexpectedNumberOfElementsException;

public abstract class PersistenceHelper<T> implements IPersistenceHelper<T> {

	@Override
	public T getSingleResult(StringBuilder queryBuilder, Connection connection) throws SQLException, UnexpectedNumberOfElementsException {
		
		T entity = this.getSingleResult(queryBuilder, new LinkedList<Object>(), connection);
		
		return entity;
	}

	@Override
	public T getSingleResult(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException, UnexpectedNumberOfElementsException {
		
		List<T> entityList = this.getList(queryBuilder, queryParams, connection);
		
		if(entityList.size() != 1) {
			String errorMsg = MessageFormat.format(Globals.stringsBundle.getString("error.unexpected.number.elements"), entityList.size());
			
			throw new UnexpectedNumberOfElementsException(errorMsg);
		}
		
		T entity = entityList.get(0);
		
		return entity;
	}

	@Override
	public List<T> getList(StringBuilder queryBuilder, Connection connection) throws SQLException {
		return this.getList(queryBuilder, new LinkedList<Object>(), connection);
	}

	@Override
	public List<T> getList(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException {
		List<T> entityList = new LinkedList<T>();
		
		PreparedStatement preparedStatement = null;
		try {
			int j = 0;
			
			preparedStatement = connection.prepareStatement(queryBuilder.toString());
			
			for(int i = 0; i < queryParams.size(); i++) {
				preparedStatement.setObject(++j, queryParams.get(i));
			}
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				entityList.add(toEntity(resultSet));
			}
			
			
		} finally {
			if(preparedStatement != null) {				
				preparedStatement.close();
			}
		}
		
		return entityList;
	}

	@Override
	public int persist(StringBuilder queryBuilder, List<Object> queryParams, Connection connection) throws SQLException {
		int insertedRows = -1;
		
		PreparedStatement preparedStatement = null;
		try {
			int j = 0;
			preparedStatement = connection.prepareStatement(queryBuilder.toString());
			
			for(int i = 0; i < queryParams.size(); i++) {
				if(queryParams.get(i) instanceof Date) {
					preparedStatement.setTimestamp(++j, new java.sql.Timestamp(((Date)queryParams.get(i)).getTime()));
				} else {
					preparedStatement.setObject(++j, queryParams.get(i));
				}
			}
			
			insertedRows = preparedStatement.executeUpdate();
			
		} finally {
			if(preparedStatement != null) {			
				preparedStatement.close();
			}
		}
		
		return insertedRows;
	}
	
	/**
	 * Reads a row from the resultSet and builds an entity T
	 * 
	 * @param resultSet the resultSet to build the entity from
	 * 
	 * @return an entity T
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public abstract T toEntity(ResultSet resultSet) throws SQLException;

}
