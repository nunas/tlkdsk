package com.talkdesk.technicaltests.backendtest.core.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.talkdesk.technicaltests.backendtest.core.common.Globals;
import com.talkdesk.technicaltests.backendtest.core.enums.CallTypeEnum;
import com.talkdesk.technicaltests.backendtest.core.util.StringUtils;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author nmoliveira
 */
@Schema(name="Call", description="Model representing the Call database entity")
public class Call {
	
	public static final String TABLE_NAME = "call";
	
	public static final class COLUMNS {
		//column names
		public static final String ID = "ID";
		public static final String CALLER_NUMBER = "CALLER_NUMBER";
		public static final String CALLEE_NUMBER = "CALLEE_NUMBER";
		public static final String CALL_TYPE = "CALL_TYPE";
		public static final String START_DATE = "START_DATE";
		public static final String END_DATE = "END_DATE";
		
		//column max length
		public static final int CALLER_NUMBER_MAX_LENGTH = 32;
		public static final int CALLEE_NUMBER_MAX_LENGTH = 32;
	}
	
	public static final String INSERT_STATEMENT = 
			"INSERT INTO " + TABLE_NAME + "(" 
					+ COLUMNS.CALLER_NUMBER + "," 
					+ COLUMNS.CALLEE_NUMBER + "," 
					+ COLUMNS.CALL_TYPE + "," 
					+ COLUMNS.START_DATE + "," 
					+ COLUMNS.END_DATE
					+ ")"
					+ " VALUES(?,?,?,?,?)";
	
	public static final String DELETE_STATEMENT = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMNS.ID + " = ?";
	
	private long id;
	private String callerNumber;
	private String calleeNumber;
	private CallTypeEnum callType;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Globals.TIMESTAMP_FORMAT)
	private Date startDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Globals.TIMESTAMP_FORMAT)
	private Date endDate;
	
	public Call() {
		
	}
	
	/**
	 * Builds a call entity based on a resultSet row
	 * 
	 * @param resultSet the resultSet holding a call entity
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public Call(ResultSet resultSet) throws SQLException {
		this.id = resultSet.getLong(Call.COLUMNS.ID);
		this.callerNumber = resultSet.getString(Call.COLUMNS.CALLER_NUMBER);
		this.calleeNumber = resultSet.getString(Call.COLUMNS.CALLEE_NUMBER);
		this.callType = CallTypeEnum.valueOf(resultSet.getString(Call.COLUMNS.CALL_TYPE));
		this.startDate = resultSet.getTimestamp(Call.COLUMNS.START_DATE);
		this.endDate = resultSet.getTimestamp(Call.COLUMNS.END_DATE);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Schema(description = "The caller number", maxLength = 32, required = true, example = "00351910000000")
	public String getCallerNumber() {
		return callerNumber;
	}

	public void setCallerNumber(String callerNumber) {
		this.callerNumber = callerNumber;
	}

	@Schema(description = "The callee number", maxLength = 32, required = true, example = "00351930000000")
	public String getCalleeNumber() {
		return calleeNumber;
	}

	public void setCalleeNumber(String calleeNumber) {
		this.calleeNumber = calleeNumber;
	}

	@Schema(description = "The call type", required = true, example = "OUTBOUND")
	public CallTypeEnum getCallType() {
		return callType;
	}

	public void setCallType(CallTypeEnum callType) {
		this.callType = callType;
	}

	@Schema(description = "The call start date", required = true, example = "2019-11-14 09:43:42")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Schema(description = "The call end date", required = true, example = "2019-11-14 09:47:11")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Validates {@code call} entity
	 * 
	 * @param call the call entity to validate
	 * 
	 * @return an error message String if entity is invalid, null otherwise
	 */
	public static String validate(Call call) {
		String errorMsg = null;
		
		if(
					StringUtils.isEmptyOrWhitespace(call.getCallerNumber()) 
				|| 	StringUtils.isEmptyOrWhitespace(call.getCalleeNumber()) 
				|| 	call.getCallType() 	== null
				|| 	call.getStartDate() == null
				|| 	call.getEndDate() 	== null
		) {
			errorMsg = Globals.stringsBundle.getString("error.call.null.values");
		} else {
			if(call.getCallerNumber().length() > COLUMNS.CALLER_NUMBER_MAX_LENGTH || call.getCalleeNumber().length() > COLUMNS.CALLEE_NUMBER_MAX_LENGTH) {
				errorMsg = Globals.stringsBundle.getString("error.call.too.long.values");
			} else if(call.getStartDate().after(call.getEndDate())) {
				errorMsg = Globals.stringsBundle.getString("error.call.start.after.end");
			}
		}
		
		return errorMsg;
	}
	
	public List<Object> asParamList() {
		List<Object> currentParams = new LinkedList<Object>();
		
		currentParams.add(this.getCallerNumber());
		currentParams.add(this.getCalleeNumber());
		currentParams.add(this.getCallType().name());
		currentParams.add(this.getStartDate());
		currentParams.add(this.getEndDate());
		
		return currentParams;
	}
}
