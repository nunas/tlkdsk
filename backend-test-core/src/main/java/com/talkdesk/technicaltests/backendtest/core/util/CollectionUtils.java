package com.talkdesk.technicaltests.backendtest.core.util;

import java.util.Collection;

public class CollectionUtils {
	
	public static boolean hasContents(@SuppressWarnings("rawtypes") Collection collection) {
		return collection != null && collection.size() > 0;
	}

}
