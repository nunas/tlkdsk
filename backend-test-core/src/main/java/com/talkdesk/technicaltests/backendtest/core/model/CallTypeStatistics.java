package com.talkdesk.technicaltests.backendtest.core.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.talkdesk.technicaltests.backendtest.core.enums.CallTypeEnum;

public class CallTypeStatistics {
	
	@JsonIgnore
	private Date startDate;
	
	private CallTypeEnum callType;
	
	private String totalTime;
	
	private long totalCalls;
	
	public CallTypeStatistics() {
		
	}
	
	public CallTypeStatistics(ResultSet resultSet) throws SQLException {
		int i = 0;
		
		this.startDate = resultSet.getDate(++i);
		this.callType = CallTypeEnum.valueOf(resultSet.getString(++i));
		this.totalTime = resultSet.getString(++i);
		this.totalCalls = resultSet.getLong(++i);
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public CallTypeEnum getCallType() {
		return callType;
	}

	public void setCallType(CallTypeEnum callType) {
		this.callType = callType;
	}
	
	public String getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}

	public long getTotalCalls() {
		return totalCalls;
	}

	public void setTotalCalls(long totalCalls) {
		this.totalCalls = totalCalls;
	}
	
}
