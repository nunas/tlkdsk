package com.talkdesk.technicaltests.backendtest.core.processor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.talkdesk.technicaltests.backendtest.core.common.Globals;
import com.talkdesk.technicaltests.backendtest.core.dao.CallTypeStatisticsDAO;
import com.talkdesk.technicaltests.backendtest.core.dao.MsisdnStatisticsDAO;
import com.talkdesk.technicaltests.backendtest.core.dao.TotalCostStatisticsDAO;
import com.talkdesk.technicaltests.backendtest.core.model.Call;
import com.talkdesk.technicaltests.backendtest.core.model.CallTypeStatistics;
import com.talkdesk.technicaltests.backendtest.core.model.MsisdnStatistics;
import com.talkdesk.technicaltests.backendtest.core.model.Statistics;
import com.talkdesk.technicaltests.backendtest.core.model.TotalCostStatistics;

/**
 * Entity that manages the call statistics operations. 
 * Also acts as a middleware between the WebService layer and the Data Access Object (DAO) layer
 * 
 * @author nmoliveira
 *
 */
public class CallStatisticsProcessor {
	
	/**
	 * Singleton Approach
	 */
	
	private static CallStatisticsProcessor callStatisticsProcessor = null;

	private CallStatisticsProcessor() {
	}

	public static CallStatisticsProcessor getInstance() {
		if (callStatisticsProcessor == null) {
			callStatisticsProcessor = new CallStatisticsProcessor();
		}
		return callStatisticsProcessor;
	}
	
	/**
	 * Builds a complex entity with a statistic compilation:
	 * 
	 * - Total call duration by type. (getCallTypeStatistics)
	 * - Total number of calls. (getCallTypeStatistics)
	 * - Number of calls by Caller Number. (getMsisdnStatistics)
	 * - Number of calls by Callee Number. (getMsisdnStatistics)
	 * - Total call cost (getTotalCostStatistics)
	 * 
	 * @return a {@code Statistics} entity holding the statistic compilation
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public Statistics getCallStatistics() throws SQLException {
		Statistics statistics = new Statistics();
		Connection connection = Globals.comboPooledDataSource.getConnection();
		
		try {			
			Map<Date, List<CallTypeStatistics>> statisticsByDate = this.getCallTypeStatistics(connection);
			Map<Date, List<MsisdnStatistics>> callerStatisticsByDate = this.getMsisdnStatistics(true, connection);
			Map<Date, List<MsisdnStatistics>> calleeStatisticsByDate = this.getMsisdnStatistics(false, connection);
			Map<Date, List<TotalCostStatistics>> totalCostStatisticsByDate = this.getTotalCostStatistics(connection);
			
			statistics.setCallTypeStatistics(statisticsByDate);
			statistics.setCallerStatistics(callerStatisticsByDate);
			statistics.setCalleeStatistics(calleeStatisticsByDate);
			statistics.setTotalCostStatistics(totalCostStatisticsByDate);
		} finally {
			connection.close();
		}
		
		return statistics;
	}
	
	/**
	 * 
	 * @param connection the connection instance for the queries
	 * 
	 * @return 
	 * 
	 * 	The total call duration and number of calls by call type.
	 * 	Values aggregated by date.
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	private Map<Date, List<CallTypeStatistics>> getCallTypeStatistics(Connection connection) throws SQLException {
		Map<Date, List<CallTypeStatistics>> statisticsByDate = new HashMap<Date, List<CallTypeStatistics>>();
		List<CallTypeStatistics> statisticsByCallType = new LinkedList<CallTypeStatistics>();
		StringBuilder queryBuilder = new StringBuilder();
		
		queryBuilder.append(" SELECT ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append(") AS start_date, ");
		queryBuilder.append(Call.COLUMNS.CALL_TYPE).append(", ");
		queryBuilder.append("SUM(").append(Call.COLUMNS.END_DATE).append("-").append(Call.COLUMNS.START_DATE).append(") AS total_time, ");
		queryBuilder.append("COUNT(*) AS total_calls");
		queryBuilder.append(" FROM ");
		queryBuilder.append(Call.TABLE_NAME);
		queryBuilder.append(" GROUP BY ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append("),");
		queryBuilder.append(Call.COLUMNS.CALL_TYPE);
		queryBuilder.append(" ORDER BY ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append(") ASC");
		
		statisticsByCallType = CallTypeStatisticsDAO.getInstance().getCallTypeStatistics(queryBuilder, connection);
		
		//aggregate statistics by date
		statisticsByDate = this.callTypeStatisticsToMap(statisticsByCallType);
		
		return statisticsByDate;
	}
	
	/**
	 * 
	 * @param isCaller true to retrieve data from the caller number, false to retrieve data from the callee number
	 * @param connection the connection instance for the queries
	 * 
	 * @return 
	 * 
	 * 	The number of calls by Caller/Callee Number.
	 * 	Values aggregated by date.
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	private Map<Date, List<MsisdnStatistics>> getMsisdnStatistics(boolean isCaller, Connection connection) throws SQLException {		
		Map<Date, List<MsisdnStatistics>> msisdnStatisticsByDate = new HashMap<Date, List<MsisdnStatistics>>();
		List<MsisdnStatistics> msisdnStatistics = new LinkedList<MsisdnStatistics>();
		StringBuilder queryBuilder = new StringBuilder();
		
		queryBuilder.append(" SELECT ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append(") AS start_date, ");
		if(isCaller) {			
			queryBuilder.append(Call.COLUMNS.CALLER_NUMBER).append(", ");
		} else {
			queryBuilder.append(Call.COLUMNS.CALLEE_NUMBER).append(", ");
		}
		queryBuilder.append("COUNT(*) AS total_calls");
		queryBuilder.append(" FROM ");
		queryBuilder.append(Call.TABLE_NAME);
		queryBuilder.append(" GROUP BY ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append("),");
		if(isCaller) {
			queryBuilder.append(Call.COLUMNS.CALLER_NUMBER);
		} else {			
			queryBuilder.append(Call.COLUMNS.CALLEE_NUMBER);
		}
		queryBuilder.append(" ORDER BY ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append(") ASC");
		
		msisdnStatistics = MsisdnStatisticsDAO.getInstance().getMsisdnStatistics(queryBuilder, connection);
		
		//aggregate statistics by date
		msisdnStatisticsByDate = this.msisdnStatisticsToMap(msisdnStatistics);
		
		
		return msisdnStatisticsByDate;
	}
	
	/**
	 * 
	 * @param connection the connection instance for the queries
	 * 
	 * @return The total call cost by date.
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	private Map<Date, List<TotalCostStatistics>> getTotalCostStatistics(Connection connection) throws SQLException {
		Map<Date, List<TotalCostStatistics>> totalCostStatisticsByDate = new HashMap<Date, List<TotalCostStatistics>>();
		List<TotalCostStatistics> totalCostStatistics = new LinkedList<TotalCostStatistics>();
		StringBuilder queryBuilder = new StringBuilder();
		
		queryBuilder.append("SELECT ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append(") as start_date,");
		queryBuilder.append(Call.COLUMNS.CALL_TYPE).append(", ");
		queryBuilder.append("SUM(extract(epoch from (").append(Call.COLUMNS.END_DATE).append("-").append(Call.COLUMNS.START_DATE).append("))) as total_seconds, ");
		queryBuilder.append("SUM(").append(Call.COLUMNS.END_DATE).append("-").append(Call.COLUMNS.START_DATE).append(") AS total_time ");
		queryBuilder.append(" FROM ");
		queryBuilder.append(Call.TABLE_NAME);
		queryBuilder.append(" GROUP BY ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append("),");
		queryBuilder.append(Call.COLUMNS.CALL_TYPE);
		queryBuilder.append(" ORDER BY ");
		queryBuilder.append("DATE(").append(Call.COLUMNS.START_DATE).append(") ASC");
		
		totalCostStatistics = TotalCostStatisticsDAO.getInstance().getTotalCostStatistics(queryBuilder, connection);
		
		//aggregate statistics by date
		totalCostStatisticsByDate = this.totalCostStatisticsToMap(totalCostStatistics);
		
		return totalCostStatisticsByDate;
	}
	
	/**
	 * Aggregates the statistics by date
	 * 
	 * @param statisticsByCallType the list to aggregate the values from
	 * 
	 * @return a Map with the statistics aggregated by date
	 */
	private Map<Date, List<CallTypeStatistics>> callTypeStatisticsToMap(List<CallTypeStatistics> statisticsByCallType) {
		Map<Date, List<CallTypeStatistics>> statisticsByDate = new HashMap<Date, List<CallTypeStatistics>>();
		CallTypeStatistics currentStatistic = null;
		
		for(int i = 0; i < statisticsByCallType.size(); i++) {
			currentStatistic = statisticsByCallType.get(i);
			
			if(statisticsByDate.get(currentStatistic.getStartDate()) != null) {
				statisticsByDate.get(currentStatistic.getStartDate()).add(currentStatistic);
			} else {
				List<CallTypeStatistics> dateStatistics = new LinkedList<CallTypeStatistics>();
				
				dateStatistics.add(currentStatistic);
				
				statisticsByDate.put(currentStatistic.getStartDate(), dateStatistics);
			}
		}
		
		return statisticsByDate;
	}
	
	/**
	 * Aggregates the statistics by date
	 * 
	 * @param msisdnStatistics the list to aggregate the values from
	 * 
	 * @return a Map with the statistics aggregated by date
	 */
	private Map<Date, List<MsisdnStatistics>> msisdnStatisticsToMap(List<MsisdnStatistics> msisdnStatistics) {
		Map<Date, List<MsisdnStatistics>> msisdnStatisticsByDate = new HashMap<Date, List<MsisdnStatistics>>();
		MsisdnStatistics currentStatistic = null;
		
		for(int i = 0; i < msisdnStatistics.size(); i++) {
			currentStatistic = msisdnStatistics.get(i);
			
			if(msisdnStatisticsByDate.get(currentStatistic.getStartDate()) != null) {
				msisdnStatisticsByDate.get(currentStatistic.getStartDate()).add(currentStatistic);
			} else {
				List<MsisdnStatistics> dateStatistics = new LinkedList<MsisdnStatistics>();
				
				dateStatistics.add(currentStatistic);
				
				msisdnStatisticsByDate.put(currentStatistic.getStartDate(), dateStatistics);
			}
		}
		
		return msisdnStatisticsByDate;
	}
	
	/**
	 * Aggregates the statistics by date
	 * 
	 * @param totalCostStatistics the list to aggregate the values from
	 * 
	 * @return a Map with the statistics aggregated by date
	 */
	private Map<Date, List<TotalCostStatistics>> totalCostStatisticsToMap(List<TotalCostStatistics> totalCostStatistics) {
		Map<Date, List<TotalCostStatistics>> totalCostStatisticsByDate = new HashMap<Date, List<TotalCostStatistics>>();
		TotalCostStatistics currentStatistic = null;
		
		for(int i = 0; i < totalCostStatistics.size(); i++) {
			currentStatistic = totalCostStatistics.get(i);
			
			if(totalCostStatisticsByDate.get(currentStatistic.getStartDate()) != null) {
				totalCostStatisticsByDate.get(currentStatistic.getStartDate()).add(currentStatistic);
			} else {
				List<TotalCostStatistics> dateStatistics = new LinkedList<TotalCostStatistics>();
				
				dateStatistics.add(currentStatistic);
				
				totalCostStatisticsByDate.put(currentStatistic.getStartDate(), dateStatistics);
			}
		}
		
		return totalCostStatisticsByDate;
	}

}
