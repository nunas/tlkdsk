package com.talkdesk.technicaltests.backendtest.core.util;

public class StringUtils {

	public static boolean isEmptyOrWhitespace(String source) {
		return source == null || source.trim().isEmpty();
	}
}
