package com.talkdesk.technicaltests.backendtest.core.processor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.common.Globals;
import com.talkdesk.technicaltests.backendtest.core.dao.CallDAO;
import com.talkdesk.technicaltests.backendtest.core.enums.CallTypeEnum;
import com.talkdesk.technicaltests.backendtest.core.model.Call;

/**
 * Entity that manages the call interactions 
 * and acts as a middleware between the WebService layer and the Data Access Object (DAO) layer
 * 
 * @author nmoliveira
 *
 */
public class CallProcessor {
	
	/**
	 * Singleton Approach
	 */
	
	private static CallProcessor callProcessor = null;

	private CallProcessor() {
	}

	public static CallProcessor getInstance() {
		if (callProcessor == null) {
			callProcessor = new CallProcessor();
		}
		return callProcessor;
	}
	
	/**
	 * For every entity, builds the insert statement and
	 * instructs the DAO layer to persist the call
	 * 
	 * @param calls the list of calls to create
	 * 
	 * @return the number of calls created
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public int createCalls(List<Call> calls) throws SQLException {
		Connection connection = Globals.comboPooledDataSource.getConnection();
		connection.setAutoCommit(false);
		
		int totalCalls = 0;
		try {			
			Call currentCall = null;
			List<Object> currentParams = null;
			
			for (int i = 0; i < calls.size(); i++) {
				currentParams = new LinkedList<Object>();
				currentCall = calls.get(i);
				
				currentParams = currentCall.asParamList();
				
				totalCalls+=CallDAO.getInstance().createCall(new StringBuilder(Call.INSERT_STATEMENT), currentParams, connection);
			}
		} catch(Exception e) {
			connection.rollback();
			
			throw e;
		} finally {
			connection.commit();
			//restore auto commit for this connection
			connection.setAutoCommit(true);
			connection.close();
		}
		
		return totalCalls;
	}
	
	/**
	 * Deletes the call entity represented by {@code id}
	 * 
	 * @param id represents the call primary key
	 * 
	 * @return 
	 * 
	 * The number of rows affected by the delete statement.
	 * Should return 1 in case of success, != 1 otherwise
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public int deleteCall(long id) throws SQLException {
		Connection connection = Globals.comboPooledDataSource.getConnection();
		connection.setAutoCommit(false);
		
		int affectedRows = 0;
		try {		 	
			//set delete parameters
			List<Object> currentParams = new LinkedList<Object>();
			
			currentParams.add(id);
			
			affectedRows = CallDAO.getInstance().deleteCall(new StringBuilder(Call.DELETE_STATEMENT), currentParams, connection);
		} catch(Exception e) {
			connection.rollback();
			
			throw e;
		} finally {
			connection.commit();
			//restore auto commit for this connection
			connection.setAutoCommit(true);
			connection.close();
		}
		
		return affectedRows;
	}
	
	/**
	 * 
     * @param type the call type {@link CallTypeEnum}
     * @param offset the index starting position
     * @param limit the maximum rows to retrieve
	 * 
	 * @return 
	 * 
	 * The list of calls, taking into account the call {@code type} 
	 * and the pagination settings {@code offset} and {@code limit} values
	 * 
	 * @throws SQLException in case of unexpected error
	 */
	public List<Call> getCalls(CallTypeEnum type, Long offset, Long limit) throws SQLException {
		Connection connection = Globals.comboPooledDataSource.getConnection();
		
		List<Call> calls = new LinkedList<Call>();
		try {
			//set query parameters
			List<Object> queryParams = new LinkedList<Object>();
			StringBuilder queryBuilder = new StringBuilder();
			
			queryBuilder.append(" SELECT ");
			queryBuilder.append(Call.COLUMNS.ID).append(", ");
			queryBuilder.append(Call.COLUMNS.CALLER_NUMBER).append(", ");
			queryBuilder.append(Call.COLUMNS.CALLEE_NUMBER).append(", ");
			queryBuilder.append(Call.COLUMNS.CALL_TYPE).append(", ");
			queryBuilder.append(Call.COLUMNS.START_DATE).append(", ");
			queryBuilder.append(Call.COLUMNS.END_DATE);
			queryBuilder.append(" FROM ");
			queryBuilder.append(Call.TABLE_NAME);
			
			if(type != null) {
				queryBuilder.append(" WHERE  ");
				queryBuilder.append(Call.COLUMNS.CALL_TYPE).append(" = ?");
				
				queryParams.add(type.name());
			}
			
			queryBuilder.append(" ORDER BY ");
			queryBuilder.append(Call.COLUMNS.ID).append(" ASC");
			
			if(limit != null) {
				queryBuilder.append(" LIMIT ?");
				
				queryParams.add(limit);
			}
			
			if(offset != null) {
				queryBuilder.append(" OFFSET ?");
				
				queryParams.add(offset);
			}
			
			calls = CallDAO.getInstance().getCalls(queryBuilder, queryParams, connection);
		} finally {
			connection.close();
		}
		
		return calls;
	}

}
