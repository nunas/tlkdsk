package com.talkdesk.technicaltests.backendtest.core.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.common.PersistenceHelper;
import com.talkdesk.technicaltests.backendtest.core.model.MsisdnStatistics;

/**
 * Data Access Object that manages the MsisdnStatistics entities
 * 
 * @author nmoliveira
 *
 */
public class MsisdnStatisticsDAO extends PersistenceHelper<MsisdnStatistics> {
	
	/**
	 * Singleton Approach
	 */
	
	private static MsisdnStatisticsDAO msisdnStatisticsDAO = null;

	private MsisdnStatisticsDAO() {
	}

	public static MsisdnStatisticsDAO getInstance() {
		if (msisdnStatisticsDAO == null) {
			msisdnStatisticsDAO = new MsisdnStatisticsDAO();
		}
		return msisdnStatisticsDAO;
	}
	
	public List<MsisdnStatistics> getMsisdnStatistics(StringBuilder queryBuilder, Connection connection) throws SQLException {
		List<MsisdnStatistics> msisdnStatistics = super.getList(queryBuilder, connection);
		
		return msisdnStatistics;
	}
	
	@Override
	public MsisdnStatistics toEntity(ResultSet resultSet) throws SQLException {
		return new MsisdnStatistics(resultSet);
	}

}
