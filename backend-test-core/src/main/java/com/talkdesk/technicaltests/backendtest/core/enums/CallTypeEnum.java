package com.talkdesk.technicaltests.backendtest.core.enums;

/**
 * Enumeration representing the call type
 * 
 * @author nmoliveira
 *
 */
public enum CallTypeEnum {
	//outside to inside
	INBOUND,
	//inside to outside
	OUTBOUND;
}
