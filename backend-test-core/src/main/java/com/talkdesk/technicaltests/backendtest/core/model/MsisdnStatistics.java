package com.talkdesk.technicaltests.backendtest.core.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MsisdnStatistics {

	@JsonIgnore
	private Date startDate;
	private String number;
	private long totalCalls;
	
	public MsisdnStatistics() {
		
	}
	
	public MsisdnStatistics(ResultSet resultSet) throws SQLException {
		int i = 0;
		
		this.startDate = resultSet.getDate(++i);
		this.number = resultSet.getString(++i);
		this.totalCalls = resultSet.getLong(++i);
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public long getTotalCalls() {
		return totalCalls;
	}
	
	public void setTotalCalls(long totalCalls) {
		this.totalCalls = totalCalls;
	}
}
