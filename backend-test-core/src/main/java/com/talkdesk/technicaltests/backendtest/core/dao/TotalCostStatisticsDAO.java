package com.talkdesk.technicaltests.backendtest.core.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.talkdesk.technicaltests.backendtest.core.common.PersistenceHelper;
import com.talkdesk.technicaltests.backendtest.core.model.TotalCostStatistics;

/**
 * Data Access Object that manages the TotalCostStatistics entities
 * 
 * @author nmoliveira
 *
 */
public class TotalCostStatisticsDAO extends PersistenceHelper<TotalCostStatistics> {
	
	/**
	 * Singleton Approach
	 */
	
	private static TotalCostStatisticsDAO totalCostStatisticsDAO = null;

	private TotalCostStatisticsDAO() {
	}

	public static TotalCostStatisticsDAO getInstance() {
		if (totalCostStatisticsDAO == null) {
			totalCostStatisticsDAO = new TotalCostStatisticsDAO();
		}
		return totalCostStatisticsDAO;
	}
	
	public List<TotalCostStatistics> getTotalCostStatistics(StringBuilder queryBuilder, Connection connection) throws SQLException {
		List<TotalCostStatistics> msisdnStatistics = super.getList(queryBuilder, connection);
		
		return msisdnStatistics;
	}
	
	@Override
	public TotalCostStatistics toEntity(ResultSet resultSet) throws SQLException {
		return new TotalCostStatistics(resultSet);
	}

}
