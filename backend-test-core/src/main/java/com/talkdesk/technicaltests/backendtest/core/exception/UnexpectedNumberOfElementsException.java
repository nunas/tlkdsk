package com.talkdesk.technicaltests.backendtest.core.exception;

public class UnexpectedNumberOfElementsException extends Exception {

	private static final long serialVersionUID = -8723850825831557657L;

	public UnexpectedNumberOfElementsException() {
		super();
	}
	
	public UnexpectedNumberOfElementsException(String message) {
		super(message);
	}
	
	public UnexpectedNumberOfElementsException(Throwable cause) {
		super(cause);
	}
	
	public UnexpectedNumberOfElementsException(String message, Throwable cause) {
		super(message, cause);
	}
}
