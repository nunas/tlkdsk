package com.talkdesk.technicaltests.backendtest.core.common;

import java.util.ResourceBundle;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class Globals {

	public static final String DATE_FORMATTER = "yyyy-MM-dd";
	public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public static ComboPooledDataSource comboPooledDataSource;
	
	public static ResourceBundle stringsBundle;
}
