package com.talkdesk.technicaltests.backendtest.core.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Statistics {

	private Map<Date, List<CallTypeStatistics>> callTypeStatistics;
	private Map<Date, List<MsisdnStatistics>> callerStatistics;
	private Map<Date, List<MsisdnStatistics>> calleeStatistics;
	private Map<Date, List<TotalCostStatistics>> totalCostStatistics;
	
	public Statistics() {
		
	}

	public Map<Date, List<CallTypeStatistics>> getCallTypeStatistics() {
		return callTypeStatistics;
	}

	public void setCallTypeStatistics(Map<Date, List<CallTypeStatistics>> callTypeStatistics) {
		this.callTypeStatistics = callTypeStatistics;
	}

	public Map<Date, List<MsisdnStatistics>> getCallerStatistics() {
		return callerStatistics;
	}

	public void setCallerStatistics(Map<Date, List<MsisdnStatistics>> callerStatistics) {
		this.callerStatistics = callerStatistics;
	}

	public Map<Date, List<MsisdnStatistics>> getCalleeStatistics() {
		return calleeStatistics;
	}

	public void setCalleeStatistics(Map<Date, List<MsisdnStatistics>> calleeStatistics) {
		this.calleeStatistics = calleeStatistics;
	}

	public Map<Date, List<TotalCostStatistics>> getTotalCostStatistics() {
		return totalCostStatistics;
	}

	public void setTotalCostStatistics(Map<Date, List<TotalCostStatistics>> totalCostStatistics) {
		this.totalCostStatistics = totalCostStatistics;
	}
}
