package com.talkdesk.technicaltests.backendtest.ws.bootstrap;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.talkdesk.technicaltests.backendtest.core.common.Globals;
import com.talkdesk.technicaltests.backendtest.core.util.StringUtils;
import com.talkdesk.technicaltests.backendtest.ws.webservice.CallWs;

import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;



/**
 * 
 * @author nmoliveira
 * 
 * Provides information on the deployment. 
 * It is simply a class that lists all JAX-RS root resources and providers.
 *
 */
@ApplicationPath("/api")
public class RestEasyServices extends Application {
	
    private static final Logger log = LogManager.getLogger(RestEasyServices.class);
	
	private static final String DATABASE_PROPERTIES_FILE = "database.properties";
	private static final String STRINGS_PROPERTIES_FILE = "strings";
 
    public RestEasyServices() {
    	try {
    		//bootstrap application here
    		
    		log.info("initializing application...");
    		
    		this.initDatabase();
    		this.initResources();
    		
    		log.info("application started successfully");
    	} catch(Throwable e) {
    		log.error("ERROR initializing application: ", e);
    	}
    }

	public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(Arrays.asList(CallWs.class, OpenApiResource.class, AcceptHeaderOpenApiResource.class));
    }
    
    /**
     * Fetches database connection parameters and initializes the connection pool
     * 
     * @throws Exception in case of unexpected error
     */
    private void initDatabase() throws Exception {
    	//load database properties
    	InputStream databasePropertiesIs = 	getClass().getClassLoader().getResourceAsStream(DATABASE_PROPERTIES_FILE);
        Properties databaseProperties = new Properties();
        databaseProperties.load(databasePropertiesIs);
        
        //connect to database
        String databaseUrl = databaseProperties.getProperty("database.url");
        String databaseDriverClass = databaseProperties.getProperty("database.driver.class");
        String databaseUser = databaseProperties.getProperty("database.username");
        String databasePwd = databaseProperties.getProperty("database.password");
        
        if(
        			!StringUtils.isEmptyOrWhitespace(databaseUrl) 
        		&&	!StringUtils.isEmptyOrWhitespace(databaseDriverClass)
        		&& 	!StringUtils.isEmptyOrWhitespace(databaseUser)
        		&&	!StringUtils.isEmptyOrWhitespace(databasePwd)) {
        	
        	Globals.comboPooledDataSource = this.createConnectionPool(databaseUrl, databaseDriverClass, databaseUser, databasePwd);
        	
        	if(Globals.comboPooledDataSource.getConnection() == null) {
        		throw new Exception("application unable to connect to database");
        	} 
        } else {
        	throw new Exception("database configuration is missing or incomplete");
        }
    }
    
    /**
     * Stores a reference to the business messages resource bundle
     */
    private void initResources() {
    	//for simplicity, use just one language
    	Locale locale = Locale.ROOT;
    	
    	Globals.stringsBundle = ResourceBundle.getBundle(STRINGS_PROPERTIES_FILE, locale);
    }
    
    /**
     * Initializes and stores a reference to the connection pool
     * 
     * @param databaseUrl
     * @param databaseDriverClass
     * @param databaseUser
     * @param databasePwd
     * 
     * @return the created connection pool
     * 
     * @throws PropertyVetoException in case of unexpected error
     */
    private ComboPooledDataSource createConnectionPool(String databaseUrl, String databaseDriverClass, String databaseUser, String databasePwd) throws PropertyVetoException {
    	ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    	
    	comboPooledDataSource.setDriverClass("org.postgresql.Driver");
    	comboPooledDataSource.setJdbcUrl(databaseUrl);
    	comboPooledDataSource.setUser(databaseUser);                                  
    	comboPooledDataSource.setPassword(databasePwd);
    	
    	return comboPooledDataSource;
    }
 
}