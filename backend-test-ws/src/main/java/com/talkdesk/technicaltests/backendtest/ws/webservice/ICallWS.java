package com.talkdesk.technicaltests.backendtest.ws.webservice;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.talkdesk.technicaltests.backendtest.core.enums.CallTypeEnum;
import com.talkdesk.technicaltests.backendtest.core.model.Call;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;

@OpenAPIDefinition(
        info = @Info(
                title = "Calls Web Service",
                version = "1.0.0",
                description = "API for the Calls Web Service",
                contact = @Contact(name = "Nuno Oliveira", email = "nunmigoliveira@gmail.com")
        ),
		tags = {
                @Tag(name = "Calls API", description = "Requested API for the take home test")
        },
        servers = {
                @Server(
                        description = "Localhost server",
                        url = "http://localhost:8080/backendtest-ws"
                	)
        }
)
@Path("/call")
@Consumes(MediaType.APPLICATION_JSON)
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
public interface ICallWS {

	@Operation(summary = "Creates a list of calls",
			method = "http://host:port/backendtest-ws/api/create",
            responses = {
                    @ApiResponse(responseCode = "200", description = "In case of success",
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN)),
                    @ApiResponse(responseCode = "400", description = "In case of business error",
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN)),
                    @ApiResponse(responseCode = "500", description = "In case of unexpected error", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN))},
    		tags = {
                 "Calls API"
            })
    @POST
    @Path("/create")
    public Response createCalls(@RequestBody(description = "List of calls to be created. At least a list with 1 element is required", required = true,
            content = @Content(
            		mediaType = MediaType.APPLICATION_JSON,
            		array = @ArraySchema(schema = @Schema(implementation = Call.class))))List<Call> calls);
    
	
	@Operation(summary = "Deletes the call uniquely identified by id",
			method = "http://host:port/backendtest-ws/api/delete/{id}",
            responses = {
                    @ApiResponse(responseCode = "200", description = "In case of success", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN)),
                    @ApiResponse(responseCode = "404", description = "In case of call not found", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN)),
                    @ApiResponse(responseCode = "500", description = "In case of unexpected error", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN))},
    		tags = {
                    "Calls API"
               })
    @DELETE
    @Path("/delete/{id}")
    public Response deleteCall(@Parameter(description = "The call unique identifier", required = true)@PathParam("id") long callId);
    
	
	@Operation(summary = "Returns all calls, taking into account the call type, and for pagination cases the offset and limit values",
			method = "http://host:port/backendtest-ws/api/allCalls",
            responses = {
                    @ApiResponse(responseCode = "200", description = "In case of success", 
                    		content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = Call.class))),
                    @ApiResponse(responseCode = "404", description = "In case of negative offset or limit values", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN)),
                    @ApiResponse(responseCode = "500", description = "In case of unexpected error", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN))},
    		tags = {
                    "Calls API"
               })
    @GET
    @Path("/allCalls")
    public Response getCalls(
    		@Parameter(description = "The call type", required = false, example = "INBOUND")@QueryParam("type") CallTypeEnum type, 
    		@Parameter(description = "The first element index to be retrieved", required = false, example = "5")@QueryParam("offset") Long offset, 
    		@Parameter(description = "Maximum elements to retrieve", required = false, example = "10")@QueryParam("limit") Long limit);

	
	@Operation(summary = "Returns a statistic compilation, with the following information: "
			+ "Total call duration by type; "
			+ "Total number of calls; "
			+ "Number of calls by Caller Number;"
			+ "Number of calls by Callee Number; "
			+ "Total call cost;",
			method = "http://host:port/backendtest-ws/api/statistics",
            responses = {
                    @ApiResponse(responseCode = "200", description = "In case of success", 
                    		content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                    @ApiResponse(responseCode = "500", description = "In case of unexpected error", 
                    		content = @Content(mediaType = MediaType.TEXT_PLAIN))},
    		tags = {
                    "Calls API"
               })
    @GET
    @Path("/statistics")
    public Response getStatistics();
}