package com.talkdesk.technicaltests.backendtest.ws.webservice;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.talkdesk.technicaltests.backendtest.core.common.Globals;
import com.talkdesk.technicaltests.backendtest.core.enums.CallTypeEnum;
import com.talkdesk.technicaltests.backendtest.core.model.Call;
import com.talkdesk.technicaltests.backendtest.core.model.Statistics;
import com.talkdesk.technicaltests.backendtest.core.processor.CallProcessor;
import com.talkdesk.technicaltests.backendtest.core.processor.CallStatisticsProcessor;
import com.talkdesk.technicaltests.backendtest.core.util.CollectionUtils;

public class CallWs implements ICallWS {
	
	private static final Logger log = LogManager.getLogger(CallWs.class);
	
	private static ObjectMapper objectMapper;
	
	static {
		objectMapper = new ObjectMapper();
		
		//nice to have configs
		objectMapper.setDateFormat(new SimpleDateFormat(Globals.DATE_FORMATTER));
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
	}
	
	@Override
	public Response createCalls(List<Call> calls) {
		Response wsResponse = Response.status(Response.Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.500")).build();
		
		try {
			
			if(CollectionUtils.hasContents(calls)) {				
				//1. validate call beans. If any is invalid, don't proceed
				String msg = null;
				Call currentCall = null;
				for(int i = 0; i < calls.size(); i++) {
					currentCall = calls.get(i);
					
					msg = Call.validate(currentCall);
					
					if(msg != null) {
						msg = MessageFormat.format(msg, i);
						
						log.warn("invalid call creation: " + msg);
						break;
					}
				}
				
				//2. all calls are valid
				if(msg == null) {
					int createdCalls = CallProcessor.getInstance().createCalls(calls);
					
					msg = MessageFormat.format(Globals.stringsBundle.getString("success.call.creation"), createdCalls);
					wsResponse = Response.status(Response.Status.OK).type(MediaType.TEXT_PLAIN).entity(msg).build();
					
				} else {
					wsResponse = Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity(msg).build();
				}
			} else {
				wsResponse = Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.call.no.calls")).build();
			}
			
		} catch(Throwable e) {
			log.error("ERROR creating calls: ", e);
		}
		
		return wsResponse;
	}

	@Override
	public Response deleteCall(long callId) {
		Response wsResponse = Response.status(Response.Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.500")).build();
		
		try {			
			int affectedRows = CallProcessor.getInstance().deleteCall(callId);
			
			if(affectedRows != 1) {
				log.debug("tried to delete call with id " + callId + " but delete result affected " + affectedRows + " rows");
				
				wsResponse = Response.status(Response.Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.unexisting.call")).build();
			} else {
				wsResponse = Response.status(Response.Status.OK).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("success.call.deletion")).build();
			}
			
		} catch(Throwable e) {
			log.error("ERROR deleting call: ", e);
		}
		
		return wsResponse;
	}

	@Override
	public Response getCalls(CallTypeEnum type, Long offset, Long limit) {
		Response wsResponse = Response.status(Response.Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.500")).build();
		
		try {			
			
			if((offset != null && offset.longValue() < 0) || (limit != null && limit.longValue() < 0)) {
				wsResponse = Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.invalid.limit.offset.values")).build();
			} else {
				List<Call> calls = CallProcessor.getInstance().getCalls(type, offset, limit);
				
				String callsJson = objectMapper.writeValueAsString(calls);
				
				wsResponse = Response.status(Response.Status.OK).type(MediaType.APPLICATION_JSON).entity(callsJson).build();
			}
			
		} catch(Throwable e) {
			log.error("ERROR retrieving calls: ", e);
		}
		
		return wsResponse;
	}

	@Override
	public Response getStatistics()  {
		Response wsResponse = Response.status(Response.Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN).entity(Globals.stringsBundle.getString("error.500")).build();
		
		try {			
			
			Statistics statistics = CallStatisticsProcessor.getInstance().getCallStatistics();
			
			String statisticsJson = objectMapper.writeValueAsString(statistics);
			
			wsResponse = Response.status(Response.Status.OK).type(MediaType.APPLICATION_JSON).entity(statisticsJson).build();
	
		} catch(Throwable e) {
			log.error("fetching statistics: ", e);
		}
		
		return wsResponse;
	}
}
